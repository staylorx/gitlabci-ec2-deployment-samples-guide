# Gitlab-CI Continuous Delivery on AWS EC2 using the CodePipeline

See README.md for basic information about this tutorial

## Setting up the environment on AWS

The first step in ensuring a successful Continuous Delivery process is to set up the necessary objects on AWS in order to allow the deployment process to succeed.

### AWS AIM User
The initial requirement will be to set up an AIM user

https://console.aws.amazon.com/iam/home?region=us-east-1#users

1. Create a user
2. Attach the following permissions:
    - CodePipelineFullAccess
    - AmazonEC2FullAccess
    - AmazonS3FullAccess
    - AWSCodeDeployFullAccess
    - Inline Policy:
    ```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:*",
                "codedeploy:*",
                "ec2:*",
                "elasticloadbalancing:*",
                "iam:AddRoleToInstanceProfile",
                "iam:CreateInstanceProfile",
                "iam:CreateRole",
                "iam:DeleteInstanceProfile",
                "iam:DeleteRole",
                "iam:DeleteRolePolicy",
                "iam:GetInstanceProfile",
                "iam:GetRole",
                "iam:GetRolePolicy",
                "iam:ListInstanceProfilesForRole",
                "iam:ListRolePolicies",
                "iam:ListRoles",
                "iam:PassRole",
                "iam:PutRolePolicy",
                "iam:RemoveRoleFromInstanceProfile",
                "s3:*"
            ],
            "Resource": "*"
        }
    ]
}
```
3. Generate security credentials

**Note**: The policies listed above are very broad in scope. You may adjust to your requirements by creating custom policies that limit access only to certain recources.

**Note**: Please keep these credentials in a safe location. You will need them in a later step. 

### AWS EC2 instance & Role
#### Instance Role for CodeDeploy
https://console.aws.amazon.com/iam/home?region=us-east-1#roles

Create a new Role that will be assigned to your EC2 Instance in order to access S3,

1. Set the name according to your naming conventions (ie. `MyDeploymentAppRole`)
2. Select `Amazon EC2` in order to allow EC2 instances to run other AWS services
3. Attache the following policies:
    - AmazonEC2FullAccess
    - AmazonS3FullAccess
    - AmazonCodeDeployRole

**Note**: The policies listed above are very broad in scope. You may adjust to your requirements by creating custom policies that limit access only to certain recources.

#### Launch Instance
https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#

Click on `Launch Instance` and follow these steps:

- Select `Amazon Linux AMI 2016.03.3 (HVM), SSD Volume Type`
- Select the required instance type (t2.micro by default)
    - Next
- Select `IAM Role` to be `MyDeploymentAppRole` (based on the name created in the previous section)
    - Next
- Select Appropriate Storage
    - Next
- Tag your instance with an appropriate name (ie. `MyApp-Production-Instance`)
    - add additional tags as required
    - Next
- Configure Security group as necessary
    - Next
- Review and Launch your instance

You will be provided with the possibility to either generate or use SSH keys. Please select the appropriate applicable method.

#### Setting up instance environment
##### Install CodeDeploy Agent

Log into your newly created EC2 instance and follow the instructions:

- http://docs.aws.amazon.com/codedeploy/latest/userguide/how-to-run-agent-install.html

CodeDeploy important paths:

- CodeDeploy Deployment base directory: `/opt/codedeploy-agent/deployment-root/`
- CodeDeploy Log file: `/var/log/aws/codedeploy-agent/codedeploy-agent.log`

##### Install your project prerequisites
If your project has any prerequisites to run, make sure that you install those before running the deployment, otherwise your startup script may fail.

### AWS S3 repository
https://console.aws.amazon.com/s3/home?region=us-east-1

In this step you will need to create an S3 bucket that will be holding your deployment files.

Simply follow these steps:

1. Choose `Create Bucket`
    - Select a bucket name (ie. `my-app-codepipeline-deployment`)
    - Select a region
2. In the console for your bucket select `Properties`
    - Expand the `Versioning` menu
    - choose `Enable Versioning`

### AWS CodeDeploy
https://console.aws.amazon.com/codedeploy/home?region=us-east-1#/applications

Now that the basic elements are set, we are ready to create the Deployment application in CodeDeploy

To create a CodeDeploy deployment application follow these steps:

1. Select `Create New Application`
2. Choose an Application Name (ie. `MyApp-Production` )
3. Choose a Deployment Group Name (ie. `MyApp-Production-Fleet`)
4. Select the EC2 Instances that will be affected by this deployment - `Search by Tags`
    - Under `Key` Select `Name`
    - Under `Value` Select `MyApp-Production-Instance`
5. Under `Service Role`, Select `MyDeploymentAppRole` 
6. Click on `Create Application`

**Note**: You may assign the deployment to any relevant Tag that applied to the desired instances targeted for deployment. For simplicity's sake, only the Name Tag has been used to chose the instance previously defined.

### AWS CodePipeline
https://console.aws.amazon.com/codepipeline/home?region=us-east-1#/dashboard

The next step is to proceed with creating the CodePipeline, which is in charge of performing the connection between the S3 bucket and the CodeDeploy process.

To create a CodePipeline, follow these steps:

1. Click on `Create Pipeline`
2. Name your pipeline (ie. `MyAppDeploymentPipeline` )
    - Next
3. Set the `Source Provider` to `Amazon S3`
    - set `Amazon S3 location` to the address of your bucket and target deployment file (ie. `s3://my-app-codepipeline-deployment/myapp.zip` )
    - Next
4. Set `Build Provider` to `None` - This is already handled by Gitlab-CI as will be covered later
    - Next
5. Set `Deployment Provider` to `AWS CodeDeploy`
    - set `Application Name` to the name of your CodeDeploy Application (ie. `MyApp-Production`)
    - set `Deployment Group` to the name of your CodeDeploy Deployment Group (ie. `MyApp-Production-Fleet` ) 
    - Next
6. Create or Choose a Pipeline Service Role
    - Next
7. Review and click `Create Pipeline`

## Setting up the environment on Gitlab

Now that The AWS environment has been prepared to receive the application deployment we can proceed with setting up the CI environment and settings to ensure that the code is built and deployed to an EC2 Instance using S3, CodeDeploy and the CodePipeline.

### Gitlab Variables

In order for the deployment to work, we will need to set a few environment variables in the project repository.

In your Gitlab Project, navigate to the `Variables` area and set the following variables:

- `AWS_DEFAULT_REGION` => your AWS region
- `AWS_SECRET_ACCESS_KEY` => your AWS user credential secret key
- `AWS_ACCESS_KEY_ID` => your AWS user credential key ID
- `AWS_S3_LOCATION` => the location of your deployment zip file (ie. `s3://my-app-codepipeline-deployment/my_app.zip` )

These variables will be accessible by the scripts executed by the Gitlab-CI containers.

### Startup script

A simple startup script has been provided to allow the deployment to perform the following tasks:

- Start the application and create a PID file
- Check the status of the application through the PID file
- Stop the application

You may find this script under `deploy/extras/my_app.sh` 

### Creating gitlab-ci.yml

The `gitlab-ci.yml` file is in charge of performing the Continuous Integration tasks associated with a given commit.
It acts as a simplified group of shell scripts that are organized in stages which correspond to the different phases in your Continuous Integration steps.

For more information on the details and reference, please refer to the following two links:

-  http://docs.gitlab.com/ce/ci/quick_start/README.html
-  http://docs.gitlab.com/ce/ci/yaml/README.html

You may validate the syntax of your `gitlab-ci.yml` file at any time with the following tool: https://gitlab.com/ci/lint

For the purpose of deployment, we will cover only the last piece of the sample provided with this guide:

```yaml
deploy-job:
  # Script to run for deploying application to AWS
  script:
    - apt-get --quiet install --yes python-pip # AWS CLI requires python-pip, python is installed by default
    - pip install -U pip  # pip update
    - pip install awscli  # AWS CLI installation
    - $G build -x test -x distTar # # Build the project with Gradle
    - $G distZip  # creates distribution zip for deployment
    - aws s3 cp $BUNDLE_SRC $AWS_S3_LOCATION # Uploads the zipfile to S3 and expects the AWS Code Pipeline/Code Deploy to pick up
  # requires previous CI stages to succeed in order to execute
  when: on_success
  stage: deploy
  environment: production
  cache:
    key: "$CI_BUILD_NAME/$CI_BUILD_REF_NAME"
    untracked: true
    paths:
        - build/
  # Applies only to tags matching the regex: ie: v1.0.0-My-App-Release
  only:
    - /^v\d+\.\d+\.\d+-.*$/
  except:
    - branches
    - triggers
```

This part represents the whole job associated with the deployment following the previous, if any, C.I. stages.
 
The relevant part associated with the deployment is this:

```yaml
  # Script to run for deploying application to AWS
  script:
    - apt-get --quiet install --yes python-pip # AWS CLI requires python-pip, python is installed by default
    - pip install -U pip  # pip update
    - pip install awscli  # AWS CLI installation
    - $G build -x test -x distTar # # Build the project with Gradle
    - $G distZip  # creates distribution zip for deployment
    - aws s3 cp $BUNDLE_SRC $AWS_S3_LOCATION # Uploads the zipfile to S3 and expects the AWS Code Pipeline/Code Deploy to pick up
```

The first step involves installing the python package management system: `pip`.
`pip` is required to install AWS CLI, which is necessary to upload the deployment file to AWS S3

In this example, we are using Gradle (defined by the environment variable `$G`); Gradle provides a module to automatically Zip the deployment files. Depending on the type of project you are deploying this method will be different for generating the distribution zip file `my_app.zip`.

The `aws s3 cp $BUNDLE_SRC $AWS_S3_LOCATION` command uploads the distribution zip file to the Amazon S3 location that we defined earlier. This file is then automatically detected by CodePipeline, processed and sent to CodeDeploy. 
Finally, CodeDeploy performs the necessary tasks through the CodeDeploy agent as specified by the `appspec.yml` file.

### Creating appspec.yml

The `appspec.yml` defines the behavior to be followed by CodeDeploy once a deployment file has been received.

A sample file has been provided along with this guide along with sample scripts to be executed during the various phases of the deployment.

Please refer to the specification for the CodeDeploy AppSpec for more information on how to build the `appspec.yml` file: http://docs.aws.amazon.com/codedeploy/latest/userguide/app-spec-ref.html

### Generating the Deployment ZipFile

In order for CodeDeploy to work properly, you must create a properly generated zip file of your application.

The zip file must contain:

- Zip root
    - `appspec.yml` => CodeDeploy deployment instructions
    - deployment stage scripts 
        - provided samples would be placed in the `scripts` directory in the zip file, would require the presence `my_app.sh` script to be added at the root of your application directory (ie. `my_app` directory in the zip)
    - distribution code - in our example it would be under the `my_app` directory
    
Tools such as Gradle and Maven are capable of generating distribution zip files with certain alterations to the zip generation process.
If you do not use such a tool, you may have to instruct Gitlab-CI to generate this zip file in a different manner; this method is outside of the scope of this guide.
 
## Deploying your application to EC2

The final step in this guide is actually performing a successful deployment.

The stages of Continuous integration are defined by the rules set in the `gitlab-ci.yml`. The example provided with this guide will initiate a deploy for any reference matching the following regex: `/^v\d+\.\d+\.\d+-.*$/`.
In this case, pushing a Tag `v1.0.0-My-App-Alpha-Release` through git onto your remote Gitlab would initiate the deployment process. You may adjust these rules as applicable to your project requirements.

The `gitlab-ci.yml` example provided would perform the following jobs when detecting the Tag `v1.0.0-My-App-Alpha-Release`:
 
- build job - compile the sources
- test job - run the unit tests
- deploy-job - compile the sources, generate the distribution zip, upload zip to Amazon S3

Once the distribution zip has been uploaded to Amazon S3, the following steps happen:

- CodePipeline detects the change in the revision of the S3 zip file
- CodePipeline validates the file
- CodePipeline sends signal that the bundle for CodeDeploy is ready
- CodeDeploy executes the deployment steps 
    -  Start - initialization of the deployment
    -  Application Stop - Executes defined script for hook
    -  DownloadBundle - Gets the bundle file from the S3 repository through the CodePipeline
    -  BeforeInstall - Executes defined script for hook
    -  Install - Copies the contents to the deployment location as defined by the `files` section of `appspec.yml`
    -  AfterInstall - Executes defined script for hook
    -  ApplicationStart - Executes defined script for hook
    -  ValidateService - Executes defined script for hook
    -  End - Signals the CodePipeline that the deployment has completed successfully
    

## References
 - Gitlab-CI QuickStart: http://docs.gitlab.com/ce/ci/quick_start/README.html
 - Gitlab-CI .gitlab-ci.yml: http://docs.gitlab.com/ce/ci/yaml/README.html
 - AWS CodePipeline Walkthrough: http://docs.aws.amazon.com/codepipeline/latest/userguide/getting-started-w.html
 - Install or Reinstall the AWS CodeDeploy Agent: http://docs.aws.amazon.com/codedeploy/latest/userguide/how-to-run-agent-install.html
 - AWS CLI Getting Started - Env: http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-environment
 - AppSpec Reference: http://docs.aws.amazon.com/codedeploy/latest/userguide/app-spec-ref.html


